"""
    Quantum walk over reconfigurable waveguide array simulator.

    To use, must define following parameters:
        - Propagation constant
        - Input photons
        - Coupling coefficients.
            - Defined wrt to meters. Parameterization.
            - Sample: 5mm-1 -> 5E3
        - Length of the structure (wrt meters)

    TODO:
     - Decide if user should call simulator just once,
        or each function seperately
"""
from Simulator_Class import Simulator

# TODO
"""PATH OF PROPAGATION PLOTS"""

"""For each element in the list, there is a waveguide with that beta"""
prop_const = [0 for i in range(21)]

"""Which waveguides will have photons inserted into them? [0..n]"""
# TODO: To define which waveguide(s) your photon enters:
# input_photons[wg_number] = 1
# input_photons = [0, 1, 0]

# input_photons = [1, 1, 1]
input_photons = [0 for i in range(21)]
# input_photons[9] = 1
input_photons[10] = 1
input_photons[11] = 1
# input_photons[6] = 1
# input_photons[3] = 0
# input_photons[4] = 0


"""Coupling between each pair of adjacent waveguides"""
coupling_coeff = [5E-3 for i in range(20)]

# coupling_coeff = [7E-3 for i in range(20)]

"""Structure Length"""
# TODO Incorporate into the actual calculation
struct_length = 780e-6
# struct_length = 82e-6


simulation = Simulator(prop_const, coupling_coeff, input_photons, struct_length)

hamiltonian = simulation.generate_hamiltonian(prop_const, coupling_coeff)

# Could define a hamiltonian here.

unitary, input_photons = simulation.time_evolve(hamiltonian, input_photons, struct_length)

probability_amps = simulation.simulate(unitary, input_photons)

probabilities = simulation.get_probabilities(probability_amps)

# prob_sum = 0
# for each in probabilities:
    # prob_sum += each
# print "Sum of probs:", prob_sum, "\n"
# print "Number of probs:", len(probabilities)

simulation.plot_results(probabilities)
