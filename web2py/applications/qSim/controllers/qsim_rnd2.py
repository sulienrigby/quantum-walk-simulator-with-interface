# TODO:
#   - Maintain active waveguides
#       - Database config underway in live_post
#       - Waveguides don't reset back to default inactive status

#   - Implement basic transfer function

# TODO Later:
#   - Implement features such as:
#       - Noisy Simulations
#       - Export/Download results


def index():
    plot_pygal = URL('qsim', 'process_csv')
    title = CENTER(FORM("Configure your waveguide parameters"))

    sub = CENTER(FORM("Click a waveguide to send a photon down it.", BR()))

    # target = EMBED(_type="image/svg+xml", _src="URL('qsim', 'process_csv')")

    target = EMBED(_type="image/png", _src="URL('qsim', 'make_hamiltonian')")


    test_plot()

    # process_csv()

    # form = make_structure(db.active_waveguides)

    return dict(\
                plot_pygal = plot_pygal, \
                title = title, \
                sub = sub, \
                target = target, \
                # results = results
                )

def process_csv():
    remove_me = []
    prop_const = []
    coupling_coeffs = []
    photons = []
    processing = False
    nxt_set = 0
    if request.vars.csv_file != None and request.vars.csv_file != '':
        processing = True
        val = ""
        file = request.vars.csv_file.file

        for rows in file:
            for element in rows:
                if element != "," and element != "\r" \
                        and element != "\n" and element != "":
                    val = val + str(element)
                else:
                    if val != '':
                        prop_const.append(float(val))
                    else:
                        prop_const.append(val)
                    val = ""

    for each in prop_const:
        if each == '':
            nxt_set += 1

        if nxt_set == 1:
            remove_me.append(each)
            coupling_coeffs.append(each)

        if nxt_set > 1:
            photons.append(each)
            remove_me.append(each)

    for each in remove_me:
        prop_const.remove(each)

    empty = ''
    while empty in coupling_coeffs:
        coupling_coeffs.remove(empty)
    while empty in photons:
        photons.remove(empty)

    print '\n\ncoupling_coeffs', coupling_coeffs
    print 'prop_const', prop_const
    print 'photons', photons

    if processing:
        make_hamiltonian(coupling_coeffs, prop_const, photons)
        # print "here?"
        # return plot

def make_hamiltonian(coupling, prop_const, photons):
    import numpy as np
    import scipy.linalg as scipy
    ham = np.zeros((len(prop_const), len(prop_const)))

    for i in range(0, len(prop_const)):
        ham[i][i] = prop_const[i]
        if i == 0:
            ham[i][1] = coupling[i]

        elif i == len(prop_const) - 1:
            ham[i][i - 1] = coupling[i - 1]

        else:
            ham[i][i - 1] = coupling[i - 1]
            ham[i][i + 1] = coupling[i]

    print '\nHamiltonian:\n', ham

    unitary = scipy.expm(-1j * ham)
    print '\n unitary:\n', unitary

    # photons = np.vstack(photons)

    results = np.dot(photons, unitary)
    print '\nresults\n', results

    # Unitary . Photons == Photons . Unitary
    # results = np.dot(unitary, photons)
    # print '\nresults\n', results


    probabilities = []

    prob_sum = 0

    for each in results:
        val = np.absolute(each)**2
        probabilities.append(val)
        prob_sum += val

    print '\nprobabilities', probabilities
    print 'sum of probs:', prob_sum

    response.headers['Content-Type']='image/png'
    # return_plot(probabilities)
    # test_plot()
    return probabilities
    matplot_plot(probabilities)

    # return probabilities

    # plot = test_plot(probabilities)
    # return plot

def return_plot(probs):
    response.headers['Content-Type']='image/png'
    print 'returning here..'
    return matplot_plot([1,2,3])
    print 'made it here..'
    return matplot_plot(probabilities)


def live_post():
    # print request.vars
    # print request.vars['wg_1'] -  prints value of index wg_1

    # db = define_table('wg1', Field('name'), Field('boolean', default='False'))
    # db.wg1.insert(name='wg_1', boolean='True')

    for clicked_el in request.vars:
        myquery=(db.test_table.name==clicked_el)
        myset = db(myquery)

        if myset.count() == 0:
            db.test_table.insert(name=clicked_el, status="True")

        rows = myset.select()
        for wg in rows:
            if wg.status:
                myset.update(status="False")
            elif not wg.status:
                myset.update(status="True")

    # print_db_contents()
    # print "yup"
    # probs = process_csv()
    # test_plot(probs)

def matplot_plot(probabilities = [2, 4, 6], title='title', xlab = 'x', ylab = 'y'):
    # import matplotlib
    from matplotlib.backends.backend_agg import FigureCanvasAgg as FigureCanvas
    from matplotlib.figure import Figure
    import cStringIO

    fig = Figure()
    fig.set_facecolor('white')
    ax = fig.add_subplot(111)

    ax.set_title(title)
    ax.set_xlabel(xlab)
    ax.set_ylabel(ylab)

    x = []
    y = []

    num_guides = len(probabilities)

    for i in range(0, num_guides):
        x.append(i)
        y.append(probabilities[i])

        ell = ax.plot(x, y)

    print '\n\nx', x
    print 'y', y

    print 'ell', ell

    canvas = FigureCanvas(fig)
    stream = cStringIO.StringIO()
    canvas.print_png(stream)
    return stream.getvalue()

def test_plot():
    import pygal
    from pygal.style import CleanStyle
    response.files.append(URL('static/js/pygal-tooltips.min.js'))
    response.headers['Content-Type'] = 'image/svg+xml'

    bar_chart = pygal.Bar(style=CleanStyle)

    # probabilities = process_csv()
    # print probabilities
    # if probabilities != None:

    # bar_chart.add('Results', probabilities)
    # bar_chart.force_uri_protocol ='http'

    # return bar_chart.render()

    # bar_data = transfer()
    # bar_chart.add('Results', bar_data)


def print_db_contents():
    check_query = (db.test_table.name != "None")
    query_set = db(check_query)
    select_set = query_set.select()
    for query_results in select_set:
        print query_results
