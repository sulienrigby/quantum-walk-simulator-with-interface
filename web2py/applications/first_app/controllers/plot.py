def index():
    plot_pygal=URL('plot', 'plot_pygal')
    return dict(plot_pygal=plot_pygal)

def plot_pygal():
    response.files.append(URL('frst_controller', 'static/js/pygal-tooltips.min.js'))
    response.headers['Content-Type']='image/svg'

    import pygal
    from pygal.style import CleanStyle

    bar_chart = pygal.Bar(style=CleanStyle)

    bar_chart.add('Test', [0, 3, 5])
    return bar_chart.render_to_png()
