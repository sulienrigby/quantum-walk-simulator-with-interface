"""
TODO:

 - Transfer() function, with args as count of active wg's (fr now)
    - if, if wg1 && wg2: plot (10, 01)

https://quantumexperience.ng.bluemix.net/qstage/#/tutorial?sectionId=75a85f7e14ae3fd4329ad5c3e59466ea&pageIndex=2

 - Upload CSV option
    - Will contain identity/transfer matrix.
    - Process

"""

def index():
    # import web2py_ajax
    # exec('import numpy as np')
    # form = FORM(_id="me")
    # import pygal
    plot_pygal = URL('frst_controller', 'plot')
    title = CENTER(FORM("Configure your waveguide schematic"))

    sub = CENTER(FORM("Click a waveguide to send a photon down it"), BR())

    lhs_1 = INPUT(_type="checkbox", _name="lhs_1", _id="lhs_1")

    # wg_1 = FORM(INPUT(_type="checkbox", callback=URL('simulate'),
    # target="target", _name='wg_1', _id='wg_1'),
    # CENTER(A(IMG(_src=URL('static', 'wg_str.png')))))

    target = EMBED(_type="image/svg+xml", _src="URL('frst_controller', 'plot')")

    wg = db.waveguide1

    if request.vars.Port_1:
        this = FORM("BOO!")

    return dict(target=target, plot_pygal=plot_pygal, title = title,
                    sub=sub, wg=wg)

def second_form():
    another_component = INPUT(_name='test', _type='submit',_value='Another Component')
    return another_component


def simulate():
    # exec('import numpy as np')
    import numpy as np

    print request.vars.me

    g = np.pi * 3
    # return dict(g=g) dict captures function name (simulate)
    return g


def post():
    print response.vars
    print request.vars
    print request.post_vars
    print request.get_vars
    return request.vars.wg2

def plot():
    """
        Implement transfer function!
        Plot results
    """

    import pygal
    from pygal.style import CleanStyle
    response.files.append(URL('static/js/pygal-tooltips.min.js'))
    response.headers['Content-Type'] = 'image/svg+xml'

    bar_chart = pygal.Bar(style=CleanStyle)

    bar_chart.add('Test', [1])
    bar_chart.force_uri_protocol = 'http'
    return bar_chart.render()

def waveguides():
    db.waveguide.id.readable=True

def transfer():
    timmeh = response._vars
"""
    - check which waveguides active
    - Perform necesary function (backend call)

    - In meantime, determine basic transfer function form and equation
        -   Implement for single waveguide (so result not hax 100%)


Other:
    Gets status of port_1 (ie, this = ON|OFF - from memory.)
        this = db(db.waveguide1.Port_1).select()

        checked = [field for field in db.waveguide if db.waveguide[field] is True]

HTML:
type="image/svg+xml" src="{{=plot_pygal}}"

"""


    # for field in db.waveguide:
    #     checked = db.waveguide if db.waveguide[field] is True


    # query=((db.waveguide.boolean))
    # fields = (db.waveguide.id)
    # query=query,
    # form = SQLFORM.grid(fields=fields, query=query)
    # return dict(table=table)


    # print str(DIV(g))
    # return DIV(g)
    # test = FORM(_name='output', _value=g)
    # return dict(form=test)

# def ajaxwiki_onclick():
    # return MARKMIN(request.vars.text).xml()

# def click_function():
#     return MARKMIN(request.vars.text).xml()
    # return dict(message="hello from plot_something.py")


# form_x = FORM(title, sub, wg_1, wg_2, form, this)

# return dict(form_x=form_x)
# for this in request.vars:
#     print this

# for row in this:
#     print row.Port_1

# for this in form1:
#     if this is 'on':
#         form=LABEL("BOO!")

# for these in db.waveguide:
#     if these is 'on':
#         form=LABEL("boo")

# if db.waveguide is 'on':
#     form=LABEL("boo")

# form = request.vars

# for these in db.waveguide:
# if form1.accepts(request.vars, session):
#     if these == True:
#         this = True
#         print "hi"



"""
PyGame Drawing. Embed into webpage??
    import pygame as pyg

    pyg.init()

    DISPLAY=pyg.display.set_mode((500,400),0,32)

    WHITE=(255,255,255)
    blue=(0,0,255)

    DISPLAY.fill(WHITE)

    pyg.draw.rect(DISPLAY,blue,(200,150,100,50))


"""
