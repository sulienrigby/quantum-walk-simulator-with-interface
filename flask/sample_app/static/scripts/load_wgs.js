function load_num_wgs(wg_num){
  // Update button with number of waveguides selected.
  var drop_box = document.getElementById('dropbtn');
  drop_box.innerHTML= "Number of waveguides: " + wg_num;

  var div_to_fill = document.getElementById('waveguides');
  // Remove any existing waveguides
  clear_div(div_to_fill);

  var i;
  div_to_fill.appendChild(document.createElement("br"));
  div_to_fill.appendChild(document.createElement("br"));

  // Generate the waveguide sliders, and respective elements.
  for(i = 0; i < wg_num; i++){
    var para = document.createElement("p");
    var label = document.createElement("label");
    label.setAttribute("id", "wg_num_label");
    label.innerHTML = "" + i + "     ";
    para.appendChild(label);

    chk_box = document.createElement("input");
    chk_box.setAttribute('id', 'checkbox' + i);
    chk_box.setAttribute('type', 'checkbox');
    chk_box.setAttribute('class', 'checkbox');
    chk_box.setAttribute('name', 'photon_checkbox');
    chk_box.addEventListener('click', function(){_process_from_checkbox()});
    para.appendChild(chk_box);

    var pad_label = document.createElement("label");
    pad_label.setAttribute("id", "pad_label");
    para.appendChild(pad_label);

    var beta_slider = document.createElement("input");
    beta_slider.setAttribute('id', 'wg_slider' + i, 'type', 'text', 'data-slider-id', 'ex1Slider', 'data-slider-min', '0', 'data-slider-value', '14');
    beta_slider.setAttribute('data-slider-max', '20');
    beta_slider.setAttribute('data-slider-step', '1');
    beta_slider.setAttribute('data-slider-value', '0');
    beta_slider.setAttribute('class', 'wg_slider');

    para.setAttribute('class', 'comb_input');
    para.appendChild(beta_slider);

    var pad_label = document.createElement("label");
    pad_label.setAttribute("id", "beta_label" + i);
    pad_label.setAttribute("class", "beta_label");
    pad_label.innerHTML = "B(" + i + ") = 0"

    para.appendChild(pad_label);

    div_to_fill.appendChild(para);

    div_to_fill.appendChild(document.createElement("br"));
    div_to_fill.appendChild(document.createElement("br"));
    div_to_fill.appendChild(document.createElement("br"));

  }

  // Formatting sliders
  for(i = 0; i < wg_num; i++){
    var para = document.createElement("p");
    var slider = new Slider('#wg_slider'+i, {
      formatter: function(value) {
        return 'Beta = ' + value;
      }
    });

    // Perform a simulation when the value changes on the slider.
    var originalVal;
    var newVal;
    slider.on("slideStop", function(value){
      newVal = value;
      if (originalVal != newVal){
        get_input_data(wg_num)};
      });

    slider.on("change", function(event){
      if (event.oldValue != event.newValue && event.newValue == newVal){
        get_input_data(wg_num);
      }
    });
  };

  div_to_fill = document.getElementById('coupling');
  clear_div(div_to_fill);

  // Orinigal Sliders
  for(i = 0; i < wg_num - 1; i++){
    para = document.createElement("p");

    var coupling_slider = document.createElement("input");
    coupling_slider.setAttribute("id", "coupling" + i, 'type', 'text', 'data-slider-step', '4');
    coupling_slider.setAttribute('data-slider-orientation', 'vertical');
    // coupling_slider.setAttribute('data-slider-step', '4');
    coupling_slider.setAttribute('data-slider-min', '0');
    coupling_slider.setAttribute('data-slider-max', '50');
    coupling_slider.setAttribute('data-slider-value', '0');
    coupling_slider.setAttribute('class', 'cpl_slider');

    para.setAttribute('class', 'cpl_para');
    para.appendChild(coupling_slider);

    var couple_label = document.createElement("label");
    couple_label.setAttribute("id", "couple_label" + i);
    couple_label.innerHTML = "C(" + (i) + "," + (i + 1) + ") = 0";
    para.appendChild(couple_label);
    div_to_fill.appendChild(para);
  }

  for(i = 0; i < wg_num - 1; i++){
    var para = document.createElement("p");
    var slider = new Slider('#coupling'+i, {
      reversed : true,
    })

    // Detect any time this slider is changed
    var originalVal;
    var newVal;
    slider.on("slideStop", function(value){
      newVal = value;
      if (originalVal != newVal){
        get_input_data(wg_num)};
      });

    slider.on("change", function(event){
      if (event.oldValue != event.newValue && event.newValue == newVal){
        get_input_data(wg_num);
      }
    });
  };
}

function _process_from_checkbox(){
  var done = 0;
  var i = 0;
  while (done == 0){
    if (document.getElementById('coupling' + i) == null){
        done = i + 1;
    }
    i++;
  }
  get_input_data(done);
}

function get_input_data(wg_num){
  // console.log(wg_num);
  var beta = new Array(wg_num);
  var coupling = new Array(wg_num - 1);
  var photons = new Array(wg_num);

  var length = document.getElementById("wg_len");
  if (length.value == "Structure Length (M)" || length.value == ""){
    var struct_length = 780e-6;
    length.value = "780e-6 (M)";
  }
  if (length.value.indexOf("(M)") == -1){
    var struct_length = length.value;
    length.value = length.value + " (M)";
  }
  else {
    var struct_length = length.value.replace("(M)", "")
  }
  var num_photons = 0;

  for (var i = 0; i < wg_num; i++){
    var element = document.getElementById('wg_slider' + i);
    beta[i] = element.value;

    var label = document.getElementById("beta_label" + i);
    label.innerHTML = "B(" + (i) + ") = " + beta[i];


    if (i < wg_num - 1){
      element = document.getElementById('coupling' + i);
      coupling[i] = element.value;

      var label = document.getElementById("couple_label" + i);
      label.innerHTML = "C(" + (i) + "," + (i + 1) + ") = " + element.value;
    }

    element = document.getElementById('checkbox' + i)
    if (element.checked == true){
      photons[i] = 1;
      num_photons++;
    }
    else{
      photons[i] = 0;
    }
  }

  if (num_photons == 0){
    // alert("Select which waveguides have photons! (checkboxes)");
  }
    else{
    $.ajax({
      type: "GET",
      async: true,
      contentType: "application/json; charset=utf-8",
      url: "/_simulate",
      data: {
        length:struct_length,
        coupling:coupling,
        beta:beta,
        photons:photons
      },
      success: function (data) {

        if (num_photons == 1){
          // console.log("Show the propagations and histogram plots.");
          var prop_plot = "";
          var breaker = "_unique_breaker_";
          var hist_plot = "";
          var break_index = data.search(breaker);

          prop_plot = data.slice(0, break_index);
          document.getElementById('prop_plot').setAttribute('src', 'data:image/png;base64,' + prop_plot + '');
          document.getElementById('prop_plot').style.display = "inline";

          hist_plot = data.slice(break_index + breaker.length, data.length);
          document.getElementById('hist_plot_sp').setAttribute('src', 'data:image/png;base64,' + hist_plot + '');
          document.getElementById('hist_plot_sp').style.display = "inline";

          document.getElementById('corr_plot').style.display = "none";
          document.getElementById('hist_plot').style.display = "none";

        }

        else if (num_photons == 2) {
          // console.log("Show correlation matrix and histogram plots.")
          document.getElementById('prop_plot').style.display = "none";
          document.getElementById('hist_plot_sp').style.display = "none";

          var corr_plot = "";
          var breaker = "_unique_breaker_";
          var hist_plot = "";
          var break_index = data.search(breaker);

          corr_plot = data.slice(0, break_index);
          document.getElementById('corr_plot').setAttribute('src', 'data:image/png;base64,' + corr_plot + '');
          document.getElementById('corr_plot').style.display = "inline";

          hist_plot = data.slice(break_index + breaker.length, data.length);
          document.getElementById('hist_plot').setAttribute('src', 'data:image/png;base64,' + hist_plot + '');
          document.getElementById('hist_plot').style.display = "inline";

        }
        else {
          document.getElementById('hist_plot').setAttribute('src', 'data:image/png;base64,' + data + '');
          document.getElementById('hist_plot').style.display = "inline";
          document.getElementById('hist_plot_sp').style.display = "none";
          document.getElementById('prop_plot').style.display = "none";
          document.getElementById('corr_plot').style.display = "none";
        }
     },
     dataType: "html"
   });
 }
}

function clear_div(div_to_clear){
  while(div_to_clear.firstChild){
    div_to_clear.removeChild(div_to_clear.firstChild);
  }
}
