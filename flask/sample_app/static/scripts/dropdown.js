function wg_num_select() {
  document.getElementById("myDropdown").classList.toggle("show");
}

function clear_dropdown(){
  var dropdowns = document.getElementsByClassName("dropdown-content");
  var i;
  for (i = 0; i < dropdowns.length; i++) {
    var openDropdown = dropdowns[i];
    if (openDropdown.classList.contains('show')) {
      var txt = document.getElementById("wg_text");
      // openDropdown.removeChild(txt);
      openDropdown.classList.remove('show');
    }
  }
}
``
// Close the dropdown if the user clicks outside of it
window.onclick = function(event) {
  if (event.target.matches('.dropbtn')) {
    var dropdowns = document.getElementById("myDropdown")
    var txt = document.getElementById("wg_text");

    if (txt != null){
      openDropdown.removeChild(txt);
    }

    var textfield = document.createElement("input");
    textfield.setAttribute("value", "other");
    textfield.setAttribute('id', 'wg_text');

    textfield.addEventListener('change', function(){
      if (this.value != "other"){
        clear_dropdown();
        load_num_wgs(this.value);
      }
    })
    dropdowns.appendChild(textfield);
  }

  else if (event.target.matches('#wg_text')) {
    var textfield = document.getElementById("wg_text");
    textfield.setAttribute('value', '');
  }

  else if (!event.target.matches('.dropbtn')) {
    var dropdowns = document.getElementsByClassName("dropdown-content");
    var i;
    for (i = 0; i < dropdowns.length; i++) {
      var openDropdown = dropdowns[i];
      if (openDropdown.classList.contains('show')) {
        var txt = document.getElementById("wg_text");
        openDropdown.removeChild(txt);
        openDropdown.classList.remove('show');
      }
    }
  }

  if (event.target.matches('#wg_len')){
    var el = document.getElementById('wg_len');
    el.setAttribute('value', '');
    el.value = "";
  }

}
