from flask import Flask
from flask import json
from flask import jsonify
from flask import render_template
from flask import request
from flask_bootstrap import Bootstrap
app = Flask(__name__)


@app.route("/")
def index():
    return render_template('qWalker.html', result = 0)

if __name__ == "__main__":
    app.run()

@app.route("/_simulate")
def sim():
    print "\n\n\n\nBack at the server\n\n\n\n"
    print 'request.args', request.args

    found = 0
    for each in request.args:
        if each == 'beta[]':
            found = 1
            break
        # print 'each', each
        # print 'request.args[each]', request.args[each]
        #
        # print 'request.args.getlist(each)', request.args.getlist(each)
        # for _ in each:
        #     print '_', _
        # for _ in request.args.getlist(each):

    if not found:
        print "\n\n\nNo data!!\n\n\n"
        return jsonify(result = 0)

    else:
        from flask import Flask, make_response
        from Simulator_Class import Simulator
        from matplotlib.backends.backend_agg import FigureCanvasAgg as FigureCanvas
        from matplotlib.figure import Figure
        beta = []
        for each in request.args.getlist('beta[]'):
            beta.append(int(each) * 1e6)

        num_wg = len(beta)

        coupling = []
        for each in request.args.getlist('coupling[]'):
            coupling.append(int(each))

        source = []
        for each in request.args.getlist('photons[]'):
            source.append(int(each))

        struct_length = [each for each in request.args.getlist('length')][0]
        print '\n\ntype(struct_length)', type(struct_length)

        struct_length = float(struct_length)
        print '\n\nstruct_length', struct_length, "\n\n"

        if not struct_length:
            struct_length = 780e-6

        # print 'struct_length', struct_length

        num_photons = 0
        real_src = []
        for i in range(num_wg):
            real_src.append(source[i])
            if source[i]:
                num_photons += 1

        # print '\n beta', beta
        # print '\n coupling', coupling
        # print '\n source', source


        simulation = Simulator(beta, coupling, real_src, struct_length)
        hamiltonian = simulation.generate_hamiltonian(beta, coupling)

        # Could let user define a hamiltonian, and use here.
        unitary, input_photons = simulation.time_evolve(hamiltonian, real_src, struct_length)
        probability_amps = simulation.simulate(unitary, input_photons)
        probabilities = simulation.get_probabilities(probability_amps)

        prob_sum = 0
        for each in probabilities:
            prob_sum += each
        print "Sum of probs:", prob_sum, "\n"


        if num_photons == 1:
            prop_plot, histogram = simulation.plot_results(probabilities)
            response = str(prop_plot) + "_unique_breaker_" + str(histogram)
            return response

        elif num_photons == 2:
            corr_matrix, histogram = simulation.plot_results(probabilities)
            response = str(corr_matrix) + "_unique_breaker_" + str(histogram)
            return response

        else:
            histogram = simulation.plot_results(probabilities)
            return histogram

# Bootstrap(app)
